# Autor Christian Odreman

Challenge Frontend MineSweeper


Dependencias base:

- Reactjs 16.7
- node-sass

# Estructura del proyecto

| - minesweeper ------------------------------------------ | Raiz del proyecto
| - - Diagrams  ------------------------------------------ | Diagrama de flujo
| - - src ------------------------------------------------ | Workdir
| - - - Assets ------------------------------------------- | Imagenes, fuentes y main styles
| - - - Components --------------------------------------- | Componentes de la app
| - - - Containers --------------------------------------- | Contenedores de la app ( Vistas )
| - - - Utils -------------------------------------------- | constantes, utilidades varias
| - - - MainStyles---------------------------------------- | Estilos globales, Variables mixings

# Descripcion
- Se genera estructura basica con Create-react-app.
- Se crean los componentes necesarios utilizando Hooks.
- Se crea el container App donde se estarian mostrando nuestra vista principal.
- Se generan los test de renderizado y se integra un CI en bitbucket para correr los test.
- Se deploya productivamente en now.sh.

# iniciar el proyecto

Es necesario tener Node instalado

- npm install
- npm start

o

- yarn install
- yarn start

# Productive view

https://minesweeper-eta.vercel.app/

# Instrucciones de juego:
- El juego inicia al abrir la pagina con una dificultad inicial de 8 x 8.
- El juego posee 1 campo para ingresar la dificultad de la matriz con un maximo de 30 x 30;
- Si el jugador desea guardar la partida, se agrega un boton de guardar y se almacena en el localStorage.
- Al ingresar, si hay una partida guardada el juego mostrara un modal consultando si desea continuar o empezar una nueva.
- Si el usuario coloca una bandera y resulta ser una bomba, al revelar el tablero, se mostrara como una "X" indicando que efectivamente habia una bomba ahi.