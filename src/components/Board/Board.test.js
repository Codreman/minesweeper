import React from 'react';
import Board from './Board';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

describe('<Board />', () => {
    it('matches snapshot', () => {
        const wrapper = shallow(<Board />);
        expect(wrapper).toMatchSnapshot();
    }); 
});