import React from 'react';
import Cell from '../Cell/Cell';
import './styles.scss';

function Board({ data=[], handleCell=()=>{}, handleContext=() => {}, selected={}, width=550 }) {
  return (
    <div className="board" style={{width: `${width}px`}}>
      {
        data.map( datarow => {
          return datarow.map(dataitem => {
            return (
              <div key={dataitem.x * datarow.length + dataitem.y}>
                <Cell
                  callback={() => handleCell(dataitem.x, dataitem.y)}
                  cMenu={(e) => handleContext(e, dataitem.x, dataitem.y)}
                  value={dataitem}
                  selected={selected}
                />
                {(datarow[datarow.length - 1] === dataitem) ? <div className="clear" /> : ""}
              </div>
            );
          })
        })
      }
    </div>
  );
}
export default Board;