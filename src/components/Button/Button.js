import React from "react";
import "./styles.scss";

function Button({
  title = "",
  callback = () => {},
  className = "button-modal primary-button"
}) {
  return (
    <div className="button-container">
      <button className={className} onClick={() => callback()}>
        {title}
      </button>
    </div>
  );
}
export default Button;
