import React from 'react';
import Button from './Button';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

describe('<Button />', () => {
    it('matches snapshot', () => {
        const wrapper = shallow(<Button />);
        expect(wrapper).toMatchSnapshot();
    }); 
});