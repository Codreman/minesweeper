import React from 'react';
import Input from './Input';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

describe('<Input />', () => {
    it('matches snapshot', () => {
        const wrapper = shallow(<Input />);
        expect(wrapper).toMatchSnapshot();
    }); 
});