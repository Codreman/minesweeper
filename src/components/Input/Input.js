import React from 'react';
import './styles.scss';
function InputNumber({value, callback = () =>{} }){
  return(
    <div className="input-container">
      <label className="dificult-title">Difcultad: </label>
      <input 
        className="input-dificult" 
        type="number" 
        value={value} 
        onChange={(e) => callback(e.target.value)} 
      />
    </div>
  );
}
export default InputNumber;