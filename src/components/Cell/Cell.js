import React, { useState, useEffect } from 'react';
import './styles.scss';

function Cell({ value = {}, callback = () => {}, cMenu = () => {}, selected={} }) {
  const [className, setClassName] = useState('cell');
  
  useEffect( () => {
    let classNameBuild = 'cell';
    
    if (!value.isRevealed)
      classNameBuild = classNameBuild + " hidden";
    
    if (value.isMine)
      classNameBuild = classNameBuild + " is-mine";
    
    if (value.isFlagged)
      classNameBuild = classNameBuild + " is-flag"

    setClassName(classNameBuild);
  },[selected]);



  const getValue = () => {
    if (!value.isRevealed)
      return value.isFlagged ? "🚩" : null;
    
    if (value.isRevealed && value.isMine && value.isFlagged)
      return "❌";
    
    if (value.isMine)
      return "💣";
    
    if (value.neighbour === 0)
      return null;
    
    return value.neighbour;
  }

  return (
    <div
      onClick={() => callback(value.x, value.y)}
      className={className}
      onContextMenu={(e) => cMenu(e, value.x, value.y)}
    >
      {getValue()}
    </div>
  );
};
export default Cell;