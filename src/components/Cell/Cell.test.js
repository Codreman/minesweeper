import React from 'react';
import Cell from './Cell';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

describe('<Cell />', () => {
    it('matches snapshot', () => {
        const wrapper = shallow(<Cell />);
        expect(wrapper).toMatchSnapshot();
    }); 
});