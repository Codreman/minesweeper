import React from 'react';
import Modal from './Modal';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

describe('<Modal />', () => {
    it('matches snapshot', () => {
        const wrapper = shallow(<Modal />);
        expect(wrapper).toMatchSnapshot();
    }); 
});