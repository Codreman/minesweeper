import React from "react";
import "./styles.scss";
import Button from "../Button/Button";


function Modal({ modalSettings = {} }) {
  let { title = "", message = "", options = [] } = modalSettings;
  return (
    <div className="modal">
      <div className="modal-container">
        <div className="modal-header">
          <h2>{title}</h2>
        </div>
        <div className="modal-content">
          <p>{message}</p>
        </div>
        <div className="modal-footer">
          <div className="buttons">
            {
              options.map(item => {
                return (
                  <Button
                    title={item.title}
                    className={item.className}
                    callback={item.callback()}
                  />
                )
              })
            }
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;

/**
  <Button
      title="Continuar"
      className="button-modal primary-button"
      callback={() => accept()}
    />
    <Button
      title="Nuevo"
      className="button-modal cancel-button"
      callback={() => cancel()}
    />
 */