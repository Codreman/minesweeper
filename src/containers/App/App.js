import React, { useState, useEffect } from 'react';
import Board from '../../components/Board/Board';
import Button from '../../components/Button/Button';
import Input from '../../components/Input/Input';
import Modal from '../../components/Modal/Modal';
import { MAX_VALUE} from '../../utils/constants';
import {
  getMines, 
  getFlags, 
  getHidden, 
  createEmptyArray,
  getNeighbours,
  traverseBoard,
  plantMines } from '../../utils/boardHelpers';
import { clearStorage, getStorage, addStorage } from '../../utils/storage';
import './App.scss';

function App() {
  const [gameSettings, setGameSettings] = useState({width:8, height:8, bombs: 10});
  const [boardData, setBoardData] = useState([]);
  const [selected, setSelected] = useState({});
  const [gameStatus, setGameStatus] = useState("In Progress");
  const [mineCount, setMineCount] = useState(10);
  const [boardWidth, setBoardWidth] = useState(gameSettings.width * 45);
  const [inputValue, setInputValue] = useState(8);
  const [modalInfo, setModalInfo] = useState({
    title: "",
    message:"",
    options: [],
    show: false
  });

  useEffect( () => {
    let savedBoard = getStorage('board');
    let { height, width, bombs } = gameSettings;
    if (savedBoard && savedBoard.board.length){
      let modalOptions = {
        title: "Aviso!",
        message:"Hay un juego guardado, desea continuar?",
        options: [
          {title: "aceptar", className:"button-modal primary-button", callback: () => handleAccept},
          {title: "Cancelar", className:"button-modal cancel-button", callback: () => handleCancel},
        ],
        show: true
      }
      showModal(modalOptions);
    }else {
      initBoardData(height, width, bombs);
    }    
  },[]);

  const initBoardData = (height, width, bombs) => {
    let data = createEmptyArray(height, width);
    data = plantMines(data, height, width, bombs);
    data = getNeighbours(data, height, width);
    setBoardData(data);
  }

  useEffect(() => {
    setGame(inputValue);
  },[inputValue]);

  const restarGame = () => {
    let { height, width, bombs } = gameSettings;
    clearStorage();
    setMineCount(bombs);
    setGameStatus('En curso');
    initBoardData(height, width, bombs);
    setModalInfo({
      title: "",
      message:"",
      options: [],
      show: false
    });
  }

  const setGame = (value) => {
    setInputValue(value);
    setBoardWidth(value * 45);
    let settings = {
      width: value,
      height: value,
      bombs: Math.round((value * value) * 0.15)
    }
    setGameSettings(settings);
    setMineCount(settings.bombs);
  }

  const handleCellClick = (x, y) => {
    let { bombs } = boardData;
    setSelected(boardData[x][y])
    if (boardData[x][y].isRevealed || boardData[x][y].isFlagged) return null;

    if (boardData[x][y].isMine) {
      let info = {
        title: "Perdiste!!",
        message: "Que pena, has perdido =("
      }
      youGameStatus(info);
    }

    let updatedData = boardData;
    updatedData[x][y].isFlagged = false;
    updatedData[x][y].isRevealed = true;

    if (updatedData[x][y].isEmpty) {
      updatedData = revealEmpty(x, y, updatedData);
    }

    if (getHidden(updatedData).length === bombs) {
      let info = {
        title: "Ganaste!!",
        message: "Felicidades has ganado!"
      }
      youGameStatus(info);
    }

    setBoardData(updatedData);
  }

  const handleContextMenu = (e, x, y) => {
    e.preventDefault();
    let updatedData = boardData;
    let mines = mineCount;

    // check if already revealed
    if (updatedData[x][y].isRevealed) return;

    if (updatedData[x][y].isFlagged) {
      updatedData[x][y].isFlagged = false;
      mines++;
    } else {
      updatedData[x][y].isFlagged = true;
      mines--;
    }

    if (mines === 0) {
      const mineArray = getMines(updatedData);
      const FlagArray = getFlags(updatedData);
      if (JSON.stringify(mineArray) === JSON.stringify(FlagArray)) {
        let info = {
          title: "Ganaste!!",
          message: "Felicidades has ganado!"
        }
        youGameStatus(info);
      }
    }

    setBoardData(updatedData);
    setMineCount(mines)
  }

  const revealBoard = () => {
    let updatedData = boardData;
    updatedData.forEach((datarow) => {
      datarow.forEach((dataitem) => {
        dataitem.isRevealed = true;
      })
    })
    setBoardData(updatedData)
  }

  const revealEmpty = (x, y, data) => {
    let area = traverseBoard(x, y, data);
    area.forEach(value => {
      if (!value.isFlagged && !value.isRevealed && (value.isEmpty || !value.isMine)) {
        data[value.x][value.y].isRevealed = true;
        if (value.isEmpty) {
          revealEmpty(value.x, value.y, data);
        }
      }
    });
    return data;
  }

  const handleInputData = (value) => {
    if (value > MAX_VALUE)
      value = MAX_VALUE;
    setGame(value);
    const bombs =  Math.round((value * value) * 0.15);
    initBoardData(value, value, bombs);
  }

  const handleAccept = () => {
    let savedBoard = getStorage('board');
    if (savedBoard && savedBoard.board.length)
      setBoardData(savedBoard.board)
      setInputValue(savedBoard.input)
    hideModal();
  }

  const handleCancel = () => {
    restarGame();
    hideModal();
  }

  const saveGame = () => {
    if(boardData.length)
      addStorage("board", {board:boardData, input:inputValue});
  }

  const youGameStatus = ({title = "", message = ""}) => {
    let modalOptions = {
      title: title,
      message:message,
      options: [
        {title: "otra!", className:"button-modal primary-button", callback: () => restarGame},
        {title: "Cancelar", className:"button-modal cancel-button", callback: () => hideModal},
      ],
      show: true
    }
    revealBoard();
    showModal(modalOptions);
    setGameStatus(title);
  }

  const showModal = (data) => {
    setModalInfo(data);
  }
  const hideModal = () => {
    setModalInfo({
      title: "",
      message:"",
      options: [],
      show: false
    });
    clearStorage();
  }

  return (
    <div className="game">
      {
        modalInfo.show && 
        <Modal 
          modalSettings={modalInfo}           
        />
      }
      <div className="game-info">
        <span className="info">Mines remaining: {mineCount}</span>
        <Input value={inputValue} callback={(val) => handleInputData(val)}/>
        <h1 className="info">{gameStatus}</h1>
        <div className="buttons-container">
          <Button
            title="Restar"
            className="button-modal secondary-button"
            callback={() => restarGame()}
          />
          <Button
            title="Guardar"
            className="button-modal primary-button"
            callback={() => saveGame()}
          />
        </div>
      </div>
      <Board 
        data={boardData} 
        handleContext={(e, x, y) => handleContextMenu(e, x, y)} 
        handleCell={(x, y) => handleCellClick( x, y) }
        width={boardWidth}
      />
    </div>
  )
}
export default App;