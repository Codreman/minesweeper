
/**
 * Función que encripta string en base64
 *
 * @param str {String}
 *
 * @return {String}
 *
 */

module.exports.encode = (str) => {

    try{

        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode('0x' + p1);
            }));

    } catch (e) {

        // Lanzamos el mensaje de error
        console.error(e.stack);

        return null;

    }

};

/**
 * Función que decodifica un string con base64
 *
 * @param str {String}
 *
 * @return {String}
 *
 */
module.exports.decode = (str) => {
    try {

        return decodeURIComponent(escape(window.atob(str)))

    } catch (e) {

        // Lanzamos el mensaje de error
        console.error(e.stack);

        return null;

    }
};
