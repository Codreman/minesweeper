export function getMines (data) {
    let mines = [];
    data.forEach(item => {
      item.forEach(dataItem => {
        if (dataItem.isMine)
          mines.push(dataItem);
      });
    });
    return mines;
}

export function getFlags (data) {
    let flags = [];
    data.forEach(item => {
      item.forEach(dataItem => {
        if (dataItem.isFlagged)
          flags.push(dataItem);
      });
    });
    return flags;
}

export function getHidden(data) {
    let hiddends = [];
    data.forEach(item => {
      item.forEach(dataItem => {
        if (dataItem.isRevealed)
          hiddends.push(dataItem);
      });
    });
    return hiddends;
}

 
export const getRandomNumber = (dimension) => {
    // return Math.floor(Math.random() * dimension);
    return Math.floor((Math.random() * 1000) + 1) % dimension;
}

export const createEmptyArray = (height, width) => {
    let data = [];
    for (let i = 0; i < height; i++) {
      data.push([]);
      for (let j = 0; j < width; j++) {
        data[i][j] = {
          x: i,
          y: j,
          isMine: false,
          neighbour: 0,
          isRevealed: false,
          isEmpty: false,
          isFlagged: false,
        };
      }
    }
    return data;
}

export const getNeighbours = (data, height, width) => {
    let updatedData = data;
    for (let i = 0; i < height; i++) {
      for (let j = 0; j < width; j++) {
        if (data[i][j].isMine !== true) {
          let mine = 0;
          const area = traverseBoard(data[i][j].x, data[i][j].y, data, height, width);
          area.forEach(value => {
            if (value.isMine) {
              mine++;
            }
          });
          if (mine === 0) {
            updatedData[i][j].isEmpty = true;
          }
          updatedData[i][j].neighbour = mine;
        }
      }
    }
    return (updatedData);
};

// looks for neighbouring cells and returns them
export const traverseBoard = (x, y, data, height, width) => {
    const el = [];

    //up
    if (x > 0) {
      el.push(data[x - 1][y]);
    }

    //down
    if (x < height - 1) {
      el.push(data[x + 1][y]);
    }

    //left
    if (y > 0) {
      el.push(data[x][y - 1]);
    }

    //right
    if (y < width - 1) {
      el.push(data[x][y + 1]);
    }

    // top left
    if (x > 0 && y > 0) {
      el.push(data[x - 1][y - 1]);
    }

    // top right
    if (x > 0 && y < width - 1) {
      el.push(data[x - 1][y + 1]);
    }

    // bottom right
    if (x < height - 1 && y < width - 1) {
      el.push(data[x + 1][y + 1]);
    }

    // bottom left
    if (x < height - 1 && y > 0) {
      el.push(data[x + 1][y - 1]);
    }

    return el;
}

export function plantMines(data, height, width, mines) {
  // plant mines on the board
  let randomx, randomy, minesPlanted = 0;

  while (minesPlanted < mines) {
    randomx = getRandomNumber(width);
    randomy = getRandomNumber(height);
    if (!(data[randomx][randomy].isMine)) {
      data[randomx][randomy].isMine = true;
      minesPlanted++;
    }
  }

  return (data);
}
