


/**
 * @desc Guarda en el localStorage
 *
 * @param key { String }
 * @param value { any }
 *
 * @return { Boolean }
 */
export const addStorage = (key, value) => {

    try {

        // Encriptamos los datos.

        /*let stringify   = typeof value !== "string" ? JSON.stringify( value ) : value,
            encode      = base64.encode( stringify );*/

        // Almacenamos los datos
        localStorage.setItem(key, JSON.stringify(value));

        return true;

    }catch (e) {

        // Lanzamos el mensaje de error
        console.error(e.stack)

        return false;

    }

}


/**
 * @desc Obtiene info del usuario
 *
 * @param key { String }
 *
 * @return { String }
 */
export const getStorage = (key) => {

    try {

        // Almacenamos los datos
        if(!localStorage.getItem(key)){
            return null;
        }else{
            // Obtengo el valor
            let value = localStorage.getItem(key);

            // Decodificamos el valor persistido en localStorage
            //let decode = base64.decode( value );

            return JSON.parse(value);
        }

    }catch (e) {

        // Lanzamos el mensaje de error
        console.error(e.stack);

        return null;

    }

};


/**
 * @desc Elimina item del localStorage
 *
 * @param key { String }
 *
 * @return { void }
 */
export const removeStorage = (key) => {
    localStorage.removeItem(key);
};

/**
 * Metodo que borra todo el storage
 * @return { Void } 
 */
export const clearStorage = () => {
    try{
        localStorage.clear();
    }catch(err){
        console.log(err.stack)
    }
}
